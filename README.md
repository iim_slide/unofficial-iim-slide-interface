# Unofficial IIM slide interface

A nodeJS wrapper for the slide API.\
Still in Beta!
```js
const iim = require('unofficial-iim-slide-interface')

const slide = new iim.Slide('myDeviceID', 'The slide\'s ip')

// Get information about this slide
slide.getInfo()

// @param position :number from 0 to 1
// Set the position of the curtains
slide.setPos(position)

// Slide finds out how wide the curtains are, by bumping into both ends of the rail
// This is comparable to the homing sequence of a 3d printer
slide.calibrate()

// If the slide is moving, stop the movement
slide.stop()

// Tell slide to connect to a different network
slide.setWifi(ssid, password)

// end Slide() class

// If you don't know the ip of your slide you can use this function:
// Search for slide on the current network
// @return When found: a Slide() class. When not found: undefined
iim.findSlideByDeviceId(deviceID)
```

## Example usage
### Setup a slide
To use this package the slide you want to use must have local mode enabled. In this mode the slide will not communicate with IIM's servers. It will only listen for commands on your local network.\
Enable this mode:\
1. Plug the slide in.
2. Press the reset button (next to the power plug) 2 times for 0.5 seconds with a pause of 0.5 seconds.
3. The light should flash quickly 5 times.

### Onboard the slide
After the setup the slide will create a network to which you can connect to give it instructions like tell slide what your home network is.
1. Connect to the slide's network with your current machine
2. run:
```js
const iim = require('unofficial-iim-slide-interface')
// The Slide's IP is always 192.168.4.1 when it's hosting its own network
const slide = new iim.Slide('myDeviceID', '192.168.4.1'); 
(async () => {
	const response = await slide.setWifi('mySSID (wifi network name)', 'password')
	console.log(response)
})()
```
3. Slide should reboot and connect to the specified network.
\

Done. slide is now set up.\
You can run this example to test:
```js
const iim = require('unofficial-iim-slide-interface');

(async () => {
	const slide = await iim.findSlideByDeviceId('44UhBcsX')
	if (!slide) {
		console.log('Slide not found')
	}
	else {
		console.log('Found slide\'s ip', slide.ip)
	}
	const resp = await slide.setPos(Math.random())
	console.log(resp)
})()
```
