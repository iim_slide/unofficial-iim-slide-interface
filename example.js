const iim = require('./interface.js');

(async () => {
	const slide = await iim.findSlideByDeviceId('44UhBcsX')
	if (!slide) {
		console.log('Slide not found')
	}
	else {
		console.log('Found slide\'s ip', slide.ip)
	}
	const resp = await slide.setPos(Math.random())
	console.log(resp)
})()
