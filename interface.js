const DigestFetch = require('digest-fetch')
const localDevices = require('local-devices')
const EventEmitter = require('events')

async function findHosts() {
	let hosts = await localDevices()
	// TODO: hostname might be different
	// slides = slides.filter((host) => host.name == 'espressif.lan' || host.name == '_gateway ')
	hosts = hosts.map((host) => { return { ip: host.ip, mac: host.mac } })
	return hosts
}

// Search for slide on the current network
// @return When found: a Slide() class. When not found: undefined
async function findSlideByDeviceId(deviceID) {
	async function hostToSlide(deviceID, ip, hostsEvent) {
		const slide = new Slide(deviceID, ip)
		let stop = await slide.stop()
		if (stop.response == 'success') {
			hostsEvent.emit('slide', slide)
		}
		hostsEvent.emit('slide', null)
	}
	return new Promise(async (resolve, reject) => {
		const hostsEvent = new EventEmitter()
		let hosts = await findHosts()
		let hostsLeft = hosts.length
		hosts = hosts.map((host) => hostToSlide(deviceID, host.ip, hostsEvent))
		hostsEvent.on('slide', (slide) => {
			hostsLeft--
			if (slide) {
				hostsEvent.removeAllListeners()
				resolve(slide)
			}
			if (hostsLeft == 0) {
				resolve(undefined)
			}
		})
	})
}

async function slidePost(ip, deviceID, urlPath, body) {
	const client = new DigestFetch('user', deviceID, { algorithm: 'MD5' })
	const opt = {
		method: 'POST',
		headers: {
			"Content-Type": "application/json",
			"X-Requested-With": "XMLHttpRequest"
		},
		redirect: 'follow',
	}
	if (body) {
		opt.body = body
	}
	try {
		try {
			res = await client.fetch(`http://${ip}${urlPath}`, opt)
			res = await res.json()
		} catch (err) {
			res = { error: 'Invalid response from slide' }
		}
		return res
	} catch (err) {
		return { error: 'Cannot connect to slide' }
	}
}

async function setWifi(ssid, password, deviceID) {
	const slide = await findSlideByDeviceId(deviceID)
	if (!slide) {
		return ({ error: `Slide not found` })
	}
	await slide.setWifi(ssid, password)
}

class Slide {
	constructor(deviceID, ip) {
		this.deviceID = deviceID
		this.ip = ip
	}

	// Get information about this slide
	async getInfo() {
		return await slidePost(this.ip, this.deviceID, '/rpc/Slide.GetInfo', undefined)
	}

	// @param position :number from 0 to 1
	// Set the position of the curtains
	async setPos(position) {
		return await slidePost(this.ip, this.deviceID, '/rpc/Slide.SetPos', JSON.stringify({ pos: position }))
	}

	// Slide finds out how wide the curtains are, by bumping into both ends of the rail
	// This is comparable to the homing sequence of a 3d printer
	async calibrate() {
		return await slidePost(this.ip, this.deviceID, '/rpc/Slide.Calibrate', undefined)
	}

	// If the slide is moving, stop the movement
	async stop() {
		return await slidePost(this.ip, this.deviceID, '/rpc/Slide.Stop', undefined)
	}

	// Tell slide to connect to a different network
	async setWifi(ssid, password) {
		const body = JSON.stringify({ ssid, pass: password })
		return await slidePost(this.ip, '/rpc/Slide.Config.WiFi', body)
	}
}

module.exports = { Slide, findSlideByDeviceId, findSlides: findHosts }
